//console.log('hello')

(function () { 
 
    // Create object that have the context information about the field that we want to change it's output render  
    var ctx = {};
   
    ctx.OnPreRender = modifyHeaderData;
    ctx.Templates = {}; 
    ctx.Templates.Fields = { 
        // Apply the new rendering for Priority field on List View 
        "wf_status": { "View": priorityFiledTemplate } 
    }; 
    
    SPClientTemplates.TemplateManager.RegisterTemplateOverrides(ctx); 
 
 
})(); 
 
// This function provides the rendering logic for list view 
//workflow....change name
function priorityFiledTemplate(ctx) { 
 
    
    var workflow = ctx.CurrentItem[ctx.CurrentFieldSchema.Name]; 
    

    //console.log(ctx)
    //console.log(ctx.CurrentItem)

// green colour:#cab023
 
    // Return html element with appropriate color based on priority value 
    switch (workflow) { 
        case "not started": 
            return "<span style='color :#f00'><strong>Not Started</span>"; 
            break; 
        case "running": 
            return "<span style='color :lightGreen'>Running</span>"; 
    } 
} 

//change Display Name for Entry and Change Request for 'Status' for exit leave it
//no change in Display Name for Exit Request -> if statement inside modifyHeaderData

function modifyHeaderData(renderCtx)
{
    if (renderCtx.view != "{7D7DF055-572A-4A3A-BB0F-DFDB5B075995}") {
        var viewTitle = renderCtx.viewTitle;
        var linkTitleField = renderCtx.ListSchema.Field[6];
        linkTitleField.DisplayName = 'Status';
    } else modifyHeaderData = false
    
//    console.log(renderCtx)
}

