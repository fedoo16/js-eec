//
// Append jquery
//
ExecuteOrDelayUntilScriptLoaded(appendJquery, 'SP.js');
function appendJquery() {
    var script = document.createElement('script');
    script.src = '/SiteAssets/js/vendor/jquery-2.1.4.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName('head')[0].appendChild(script);
}

Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(gettingStarted);

function gettingStarted() {
	addEventListeners();
    removeFields();
}

function checkReminder() {
    if ($("#Date_x0020_Evidence_e0186e1a-7569-4166-988b-131dae20bb11_\\$DateTimeFieldDate").val().length > 0 && 
    $("#Reminder_x0020_Email_x0020_Adres_3e5c48ae-43f7-4552-9314-4e379a5bc591_\\$ClientPeoplePicker_EditorInput").val().length <= 0) {
        alert("If you do not enter a Reminder Email Adress, the Reminder won't work!");
    }
}

function removeFields() {
    $('td.ms-formlabel:contains("Content Type")').parent().remove();
}

var checkBox;
function addEventListeners() {
	checkBox = document.getElementById("Automatically_x0020_Start_x0020__1881976b-87b4-403b-99e4-28f3cd6d4fb5_$BooleanField");
	if (checkBox) {
		checkBox.addEventListener("change", checkMandatory);
	} else {
		console.log("el is null");
	}
    
    $("#ctl00_ctl33_g_73460733_e733_4765_8675_cca4e1734de3_ctl00_toolBarTbl_RightRptControls_ctl00_ctl00_diidIOSaveItem").on("click keypress", checkReminder);
}

function checkMandatory() {	
	var errorMessage = "Following fields are mandatory to start the entry process:";
	var emptyBool = false;
	
	//First Name
	if (!document.getElementById("FirstName_4a722dd4-d406-4356-93f9-2550b8f50dd0_$TextField").value) {
		errorMessage += "\nFirst Name";
		emptyBool = true;
	}
	
	//Last Name 
	if (!document.getElementById("Title_fa564e0f-0c70-4ab9-b863-0177e6ddd247_$TextField").value) {
		errorMessage += "\nLast Name";
		emptyBool = true;
	}
	
	//First Name (with special characters - local alphabet)
	if (!document.getElementById("First_x0020_Name_x0020__x0028_wi_e7b0fea8-50ba-4bc5-90d8-726c4e468fae_$TextField").value) {
		errorMessage += "\nFirst Name (with special characters - local alphabet)";
		emptyBool = true;
	}
	
	//Last Name (with special characters - local alphabet)
	if (!document.getElementById("Last_x0020_Name_x0020__x0028_wit_cb308abf-45c7-4884-a3b6-aae25201b3a7_$TextField").value) {
		errorMessage += "\nLast Name (with special characters - local alphabet)";
		emptyBool = true;
	}
	
	//Date of Birth 
	if (!document.getElementById("Date_x0020_of_x0020_Birth_805e4b4d-b880-43de-a1d9-3871370aadc9_$DateTimeFieldDate").value) {
		errorMessage += "\nDate of Birth";
		emptyBool = true;
	}
	
	//Date of Entry 
	if (!document.getElementById("Date_x0020_of_x0020_Entry_bec14e92-17c8-4019-8392-a8781a74340a_$DateTimeFieldDate").value) {
		errorMessage += "\nDate of Entry";
		emptyBool = true;
	}
	
	//Job Title
	if (!document.getElementById("Function_$input").value) {
		errorMessage += "\nJob Title";
		emptyBool = true;
	}
	
	//Professional Category 
	if (!document.getElementById("Professional_x0020_Category_$input").value) {
		errorMessage += "\nProfessional Category";
		emptyBool = true;
	}
	
	//Position 
	if (!document.getElementById("Position_$input").value) {
		errorMessage += "\nPosition";
		emptyBool = true;
	}
	
	//Payroll Status 
	if (!document.getElementById("Payroll_x0020_Status_$input").value) {
		errorMessage += "\nPayroll Status";
		emptyBool = true;
	}
	
	//Agreed Working Hours per Week 
	if (!document.getElementById("Agreed_x0020_Hours_x0020_of_x002_9735d0e3-b0df-4595-9a99-cf07056fee45_$NumberField").value) {
		errorMessage += "\nAgreed Working Hours per Week";
		emptyBool = true;
	}
	
	//Office City 
	if (!document.getElementById("Office_x0020_Location_$input").value) {
		errorMessage += "\nOffice City";
		emptyBool = true;
	}
	
	//Office Adress 
	if (!document.getElementById("Office_x0020_Location_x0020__x00_a9ea2844-8693-48fc-aae4-160ba1d81ebe_$TextField").value) {
		errorMessage += "\nOffice Adress";
		emptyBool = true;
	}
	
	//Team 1 
	if (!document.getElementById("Team_x0020_1_$input").value) {
		errorMessage += "\nTeam 1";
		emptyBool = true;
	}
	
	//Office 1
	if (!document.getElementById("Office_x0020_1_$input").value) {
		errorMessage += "\nOffice 1";
		emptyBool = true;
	}
	
	//Office 1 ID 
	if (!document.getElementById("Office_x0020_1_x0020_ID_$input").value) {
		errorMessage += "\nOffice 1 ID";
		emptyBool = true;
	}
	
	//PG 1 
	if (!document.getElementById("PG_x0020_1_$input").value) {
		errorMessage += "\nPG 1";
		emptyBool = true;
	}
	
	//PG 1 ID 
	if (!document.getElementById("PG_x0020_1_x0020_ID_$input").value) {
		errorMessage += "\nPG 1 ID";
		emptyBool = true;
	}
	
	if (emptyBool && checkBox.checked == true) {
		checkBox.checked = false;
		alert(errorMessage);
	}
}