console.log("custom actions demo 2")
//function enables or disables ribbon action buttons depeneding on a us
$(document).ready(function(){

    isCurrentUserMemberOfGroup = function(){
        var deferred = $.Deferred();

        var userIsInGroup = false;
        console.log(userIsInGroup);
        $.ajax({
            async: false,
            headers: { "accept": "application/json; odata=verbose" },
            method: "GET",
            url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/currentuser/groups",
            success: function (data) {
                data.d.results.forEach( function (value) {
                    if (value.Title == "Excel Services Viewers") {               
                         userIsInGroup = true;
                        console.log(userIsInGroup);
                    }
                });
            },
            error: function (response) {
                console.log(response.status);
            },
        
        }); 
        return deferred.promise();
    }
    
    
    if(userIsInGroup === false){
         function blockActionBtns(){
            $('<link>').attr('rel','stylesheet')
            .attr('type','text/css')
            .attr('href','http://partner.schoenherr.eu/SiteAssets/styles/hideRibbonButtons.css')
            .appendTo('head');
         }
         blockActionBtns();
    
        //  console.log('disable action buttons')  
    }else{
         console.log('enable action buttons')  
    }
    
})


//$(document).ready(function(){
    ExecuteOrDelayUntilScriptLoaded(function(){
        console.log("hello")
        isCurrentUserMemberOfGroup()  
    }, "sp.js");
///})
   
