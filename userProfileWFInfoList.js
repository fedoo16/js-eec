//
// Append jquery
//
var script = document.createElement('script');
script.src = '/SiteAssets/js/vendor/jquery-2.1.4.min.js';
script.type = 'text/javascript';
document.getElementsByTagName('head')[0].appendChild(script);

Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(gettingStarted);
 
function gettingStarted() {
    var tbody = $("table[summary='User Profile WF Info List'] > tbody");
    for (var i = 1; i < tbody.length; i = i + 2) {
        $(tbody[i]).find("td.ms-gb")[0].childNodes[1].nodeValue = ": " + $($(tbody[i+1]).find("a.ms-listlink.ms-draggable")[0]).text() + " ";
    }
}