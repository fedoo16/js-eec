
(function () { 
 
    // Create object that have the context information about the field that we want to change it's output render  
    var ctx = {};
   
    ctx.OnPreRender = modifyHeaderData;
    ctx.Templates = {}; 
    ctx.Templates.Fields = { 
        // Apply the new rendering for Priority field on List View
        //internal name for wf_status for exit requests is "x" 
        "wf_status": { "View": priorityFiledTemplate },
        "x": { "View": priorityFiledTemplate }  
    }; 
    // console.log(ctx.Templates)
    // console.log(ctx.Templates.Fields)

    SPClientTemplates.TemplateManager.RegisterTemplateOverrides(ctx); 
 
 
})(); 
 
// This function provides the rendering logic for list view 
//workflow....change name
function priorityFiledTemplate(ctx) { 
 
    
    var workflow = ctx.CurrentItem[ctx.CurrentFieldSchema.Name]; 
    console.log(workflow);
    console.log(ctx.CurrentItem);
 
    // Return html element with appropriate color based on priority value 
    switch (workflow) { 
        case "not started": 
            return "<span style='color :#f00'><strong>Not Started</span>"; 
            break; 
        case "running": 
            return "<span style='color :lightGreen'>Running</span>"; 
    } 
} 

//change Display Name for Entry, Change and Exit Request for 'Status' 
//no change in Display Name for Exit Request -> if statement inside modifyHeaderData

function modifyHeaderData(renderCtx)
{
    if (renderCtx.view == "{6ef8b922-a2f1-4567-8aef-f08894554b78}" || "{383a8c5d-f219-498e-b02d-b787d943707e}" || "{7d7df055-572a-4a3a-bb0f-dfdb5b075995}" || "tbod93-2__") {
        var viewTitle = renderCtx.viewTitle;
        var linkTitleField = renderCtx.ListSchema.Field[6];
        linkTitleField.DisplayName = 'Status';

    } else modifyHeaderData = false

}

