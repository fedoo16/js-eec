//
// Append jquery
//
function appendJquery() {
    var script = document.createElement('script');
    script.src = '/SiteAssets/js/vendor/jquery-2.1.4.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName('head')[0].appendChild(script);
}

//
// Check user attributes on checkbox change
//
Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(addEventListeners);

var $checkBoxBO, $checkBoxIT, $checkBoxHR;
function addEventListeners() {
    
	$checkBoxBO = $("#BO_x0020_Task_x0020_finished_59db33b3-0684-44bf-8507-bb7645282051_\\$BooleanField");
	$checkBoxBO.on("change", checkBOMandatory);
	
	$checkBoxIT = $("#IT_x0020_Task_x0020_finished_f9b5a1e7-abeb-4b8d-83b3-4a90affedfee_\\$BooleanField");
	$checkBoxIT.on("change", checkITMandatory);
	
	$checkBoxHR = $("#HR_x0020_Task_x0020_finished_797beae1-8c13-4106-8575-5b0db37eb679_\\$BooleanField");
	$checkBoxHR.on("change", checkHRMandatory);
}

function checkBOMandatory() {
	var $this = $(this);
	
	if ($this.is(':checked')) {
		var errorMessage = "Following fields are mandatory to complete the profile:";
		var emptyBool = false;
		
		if (!$("input[title='Office Adress']").val()) {
			errorMessage += "\nOffice Adress";
			emptyBool = true;
		}
		if (!$("div[title='Office City']").text().replace(/[\u200B]/g, '')) {
			errorMessage += "\nOffice City";
			emptyBool = true;
		}
		if (emptyBool) {
			$checkBoxBO.prop("checked", false);
			alert(errorMessage);
		}
	}
}

function checkITMandatory() {
	var $this = $(this);
	
	if ($this.is(':checked')) {
		var errorMessage = "Following fields are mandatory to complete the profile:";
		var emptyBool = false
		
		if (!$("input[title='User-ID']").val()) {
			errorMessage += "\nUser-ID";
			emptyBool = true;
		}
		if (!$("input[title='Initials']").val()) {
			errorMessage += "\nInitials";
			emptyBool = true;
		}
		if (!$("input[title='JurXpert #']").val()) {
			errorMessage += "\nJurXpert #";
			emptyBool = true;
		}
		if (!$("input[title='R-Code']").val()) {
			errorMessage += "\nR-Code";
			emptyBool = true;
		}
		if (!$("input[title='E-Mail Adress']").val()) {
			errorMessage += "\nE-Mail Adress";
			emptyBool = true;
		}
		if (!$("input[title='Work Phone']").val()) {
			errorMessage += "\nWork Phone";
			emptyBool = true;
		}
		if (!$("input[title='Mobile Phone']").val()) {
			errorMessage += "\nMobile Phone";
			emptyBool = true;
		}
		if (!$("input[title='Fax']").val()) {
			errorMessage += "\nFax";
			emptyBool = true;
		}
		if (emptyBool) {
			$checkBoxIT.prop("checked", false);
			alert(errorMessage);
		}
	}
}

function checkHRMandatory() {
	var $this = $(this);
	
	if ($this.is(':checked')) {
		var errorMessage = "Following fields are mandatory to complete the profile:";
		var emptyBool = false
		
		if (!$("input[title='First Name Required Field']").val()) {
			errorMessage += "\nFirst Name";
			emptyBool = true;
		}
		if (!$("input[title='Last Name Required Field']").val()) {
			errorMessage += "\nLast Name";
			emptyBool = true;
		}
		if (!$("input[title='First Name (with special characters - local alphabet)']").val()) {
			errorMessage += "\nFirst Name (with special characters - local alphabet)";
			emptyBool = true;
		}
		if (!$("input[title='Last Name (with special characters - local alphabet)']").val()) {
			errorMessage += "\nLast Name (with special characters - local alphabet)";
			emptyBool = true;
		}
		if (!$("input[title='Date of Birth']").val()) {
			errorMessage += "\nDate of Birth";
			emptyBool = true;
		}
		if (!$("input[title='Date of Entry']").val()) {
			errorMessage += "\nDate of Entry";
			emptyBool = true;
		}
		if (!$("div[title='Job Title']").text().replace(/[\u200B]/g, '')) {
			errorMessage += "\nJob Title";
			emptyBool = true;
		}
		if (!$("div[title='Professional Category']").text().replace(/[\u200B]/g, '')) {
			errorMessage += "\nProfessional Category";
			emptyBool = true;
		}
        if (!$("div[title='Position']").text().replace(/[\u200B]/g, '')) {
			errorMessage += "\nPosition";
			emptyBool = true;
		}
        if (!$("div[title='Payroll Status']").text().replace(/[\u200B]/g, '')) {
			errorMessage += "\nPayroll Status";
			emptyBool = true;
		}
        if (!$("input[title='Agreed Working Hours per Week']").val()) {
			errorMessage += "\nAgreed Working Hours per Week";
			emptyBool = true;
		}
        //
        if (!$("div[title='Team 1']").text().replace(/[\u200B]/g, '')) {
			errorMessage += "\nTeam 1";
			emptyBool = true;
		}
        if (!$("div[title='Office 1']").text().replace(/[\u200B]/g, '')) {
			errorMessage += "\nOffice 1";
			emptyBool = true;
		}
        if (!$("div[title='PG 1']").text().replace(/[\u200B]/g, '')) {
			errorMessage += "\nPG 1";
			emptyBool = true;
		}
        if (!$("div[title='PG 1 ID']").text().replace(/[\u200B]/g, '')) {
			errorMessage += "\nPG 1 ID";
			emptyBool = true;
		}
        if (!$("div[title='EP 1']").text().replace(/[\u200B]/g, '')) {
			errorMessage += "\nEP 1";
			emptyBool = true;
		}
        if (!$("div[title='Team 1 (HR)']").text().replace(/[\u200B]/g, '')) {
			errorMessage += "\nTeam 1 (HR)";
			emptyBool = true;
		}
        if (!$("div[title='Practice Area']").text().replace(/[\u200B]/g, '')) {
			errorMessage += "\nPractice Area";
			emptyBool = true;
		}
        if (!$("div[title='Agreed Currency for Remuneration/Salary']").text().replace(/[\u200B]/g, '')) {
			errorMessage += "\nAgreed Currency for Remuneration/Salary";
			emptyBool = true;
		}
        if (!$("input[title='Remuneration per Month (if \"not employed\")']").val()) {
			errorMessage += "\nRemuneration per Month (if \"not employed\")";
			emptyBool = true;
		}
        if (!$("input[title='Remuneration per Hour (if \"not employed\")']").val()) {
			errorMessage += "\nRemuneration per Hour (if \"not employed\")";
			emptyBool = true;
		}
        if (!$("input[title='Salary (if \"employed\")']").val()) {
			errorMessage += "\nSalary (if \"employed\")";
			emptyBool = true;
		}
        if (!$("input[title='Personnel Number']").val()) {
			errorMessage += "\nPersonnel Number";
			emptyBool = true;
		}
		if (emptyBool) {
			$checkBoxHR.prop("checked", false);
			alert(errorMessage);
		}
	}
}

//
// Hide/Show user attributes according to security groups
//
ExecuteOrDelayUntilScriptLoaded(checkCurrentUserMembership, 'SP.js');

function checkCurrentUserMembership() {
    appendJquery();
    
    var clientContext = new SP.ClientContext.get_current();
    var currentUser = clientContext.get_web().get_currentUser();
    clientContext.load(currentUser);

    var userGroups = currentUser.get_groups();
    clientContext.load(userGroups);
    clientContext.executeQueryAsync(onQuerySucceeded, onQueryFailed);
	
	function onQuerySucceeded() {
        var groupsEnumerator = userGroups.getEnumerator();
        
        if (userGroups.get_count() == 0) {
            console.log("No security group detected - hide all");
            removeAll();
            $('td.ms-formlabel:contains("First Name")').next().find("input").prop("disabled", true);
		    $('td.ms-formlabel:contains("Last Name")').next().find("input").prop("disabled", true);
        } 
        
		while (groupsEnumerator.moveNext()) {
			var group = groupsEnumerator.get_current();    
			if (group.get_title() == "User Management BO") {
				console.log("Hide attributes for BO");
				showBO();
				break;
			}
			else if (group.get_title() == "User Management FI") {
				console.log("Hide attributes for FI");
                showFI();
				$('td.ms-formlabel:contains("FI Task finished")').parent().show();
				break;
			}
			else if (group.get_title() == "User Management IT") {
				console.log("Hide attributes for IT");
				showIT();
				break;
			}
			else if (group.get_title() == "User Management KC") {
				console.log("Hide attributes for KC");
				showKCMA();
                $('td.ms-formlabel:contains("KC Task finished")').parent().show();
				break;
			}
			else if (group.get_title() == "User Management MA") {
				console.log("Hide attributes for MA");
                showKCMA();
                $('td.ms-formlabel:contains("MA Task finished")').parent().show();
				break;
			}
			else if (group.get_title() == "HR Members") {
				console.log("Hide attributes for HR");
				showHR();
				break;
			}
			else if (group.get_title() == "HR Owners") {
				console.log("User is Owner - show all");
				break;
			}
			else {
				console.log("No security group detected - hide all");
				removeAll();
                $('td.ms-formlabel:contains("First Name")').next().find("input").prop("disabled", true);
		        $('td.ms-formlabel:contains("Last Name")').next().find("input").prop("disabled", true);
				break;
			}
		}
		
		$('td.ms-formlabel:contains("Content Type")').parent().remove();
	}
	
	function onQueryFailed() {
		console.log("Security Check failed!");
		removeAll();
	}
	
	var disabledTermStoreCSS = { 
		"color": "#b1b1b1", 
		"border-color": "#e1e1e1", 
		"background-color": "#fdfdfd",
		"cursor": "default"
	}
	
	function showHR() {
		//--disable--
		//free fields
		$('td.ms-formlabel:contains("User-ID")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Initials")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("JurXpert Access")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("DMS Access")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("JurXpert #")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("R-Code")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("JurXpert #")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("E-Mail Adress")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Work Phone")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Mobile Phone")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Fax")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Office Adress")').next().find("input").prop("disabled", true);
		
		//term store fields + term picker img
		var $officeCity = $("div[title='Office City']");
		$officeCity.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$officeCity.parent().find('img').hide();
		
		//--remove--
		//fields	
		$('td.ms-formlabel:contains("BO Task finished")').parent().hide();
		$('td.ms-formlabel:contains("IT Task finished")').parent().hide();
		$('td.ms-formlabel:contains("FI Task finished")').parent().hide();
		$('td.ms-formlabel:contains("KC Task finished")').parent().hide();
		$('td.ms-formlabel:contains("MA Task finished")').parent().hide();
		$('td.ms-formlabel:contains("Content Type")').parent().remove();
	}
	
	function showBO() {
		//--disable--
		//free fields
		$('td.ms-formlabel:contains("First Name")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Last Name")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("First Name (with special characters - local alphabet)")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Last Name (with special characters - local alphabet)")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Title")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Additional Title")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Date of Entry")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Date of Exit")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Last Working Day")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Gender")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Job Title")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Professional Category")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Position")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("In-House Function")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Payroll Status")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Agreed Working Hours per Week")').next().find("input").prop("disabled", true);
		
		//term store fields + term picker img
		var $jobTitle = $("div[title='Job Title']");
		$jobTitle.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$jobTitle.parent().find('img').hide();

		var $professionalCategory = $("div[title='Professional Category']");
		$professionalCategory.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$professionalCategory.parent().find('img').hide();
		
		var $position = $("div[title='Position']");
		$position.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$position.parent().find('img').hide();
		
		var $payrollStatus = $("div[title='Payroll Status']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();

		//choice fields
		$('td.ms-formlabel:contains("Employment Status")').next().find("select").prop("disabled", true);
		$('td.ms-formlabel:contains("Additional Position")').next().find("select").prop("disabled", true);
		$('td.ms-formlabel:contains("Careerpath Level")').next().find("select").prop("disabled", true);
		
		//datepicker
		$('td.ms-formlabel:contains("Date of Entry")').next().find(".ms-dtinput > a").hide();
		$('td.ms-formlabel:contains("Date of Exit")').next().find(".ms-dtinput > a").hide();
		$('td.ms-formlabel:contains("Last Working Day")').next().find(".ms-dtinput > a").hide();
		
		//--remove-- 
		//fields
		$('td.ms-formlabel:contains("Date of Birth")').parent().hide();
		$('td.ms-formlabel:contains("Full-Time Equivalent (FTE)")').parent().hide();
		$('td.ms-formlabel:contains("JurXpert Access")').parent().hide();
		$('td.ms-formlabel:contains("DMS Access")').parent().hide();
		$('td.ms-formlabel:contains("Team 1")').parent().hide();
		$('td.ms-formlabel:contains("Team 1 %")').parent().hide();
		$('td.ms-formlabel:contains("Team 2")').parent().hide();
		$('td.ms-formlabel:contains("Team 2 %")').parent().hide();
		$('td.ms-formlabel:contains("Office 1")').parent().hide();
		$('td.ms-formlabel:contains("Office 1 %")').parent().hide();
		$('td.ms-formlabel:contains("Office 1 ID")').parent().hide();
		$('td.ms-formlabel:contains("Office 2")').parent().hide();
		$('td.ms-formlabel:contains("Office 2 %")').parent().hide();
		$('td.ms-formlabel:contains("Office 2 ID")').parent().hide();
		$('td.ms-formlabel:contains("PG 1")').parent().hide();
		$('td.ms-formlabel:contains("PG 1 %")').parent().hide();
		$('td.ms-formlabel:contains("PG 1 ID")').parent().hide();
		$('td.ms-formlabel:contains("PG 2")').parent().hide();
		$('td.ms-formlabel:contains("PG 2 %")').parent().hide();
		$('td.ms-formlabel:contains("EP 1")').parent().hide();
		$('td.ms-formlabel:contains("EP 1 %")').parent().hide();
		$('td.ms-formlabel:contains("EP 2")').parent().hide();
		$('td.ms-formlabel:contains("EP 2 %")').parent().hide();
		$('td.ms-formlabel:contains("Practice Area")').parent().hide();
		$('td.ms-formlabel:contains("Place of Birth")').parent().hide();
		$('td.ms-formlabel:contains("Nationality")').parent().hide();
		$('td.ms-formlabel:contains("Limited Contract Until")').parent().hide();
		$('td.ms-formlabel:contains("Exit Reason")').parent().hide();
		$('td.ms-formlabel:contains("Agreed Currency for Remuneration/Salary")').parent().hide();
		$('td.ms-formlabel:contains("Remuneration per Month (if \\"not employed\\")")').parent().hide();
		$('td.ms-formlabel:contains("Remuneration per Hour (if \\"not employed\\")")').parent().hide();
		$('td.ms-formlabel:contains("Salary (if \\"employed\\")")').parent().hide();
		$('td.ms-formlabel:contains("Salary contains Overtime Payment (\\"all-in contract\\")")').parent().hide();
		$('td.ms-formlabel:contains("Personnel Number")').parent().hide();
		$('td.ms-formlabel:contains("Planned Admittance to the Bar")').parent().hide();
		$('td.ms-formlabel:contains("Registration as Attorney")').parent().hide();
		$('td.ms-formlabel:contains("User-ID")').parent().hide();
		$('td.ms-formlabel:contains("Initials")').parent().hide();
		$('td.ms-formlabel:contains("JurXpert #")').parent().hide();
		$('td.ms-formlabel:contains("R-Code")').parent().hide();
		$('td.ms-formlabel:contains("E-Mail Adress")').parent().hide();
		$('td.ms-formlabel:contains("Work Phone")').parent().hide();
		$('td.ms-formlabel:contains("Mobile Phone")').parent().hide();
		$('td.ms-formlabel:contains("Fax")').parent().hide();
		$('td.ms-formlabel:contains("IT Task finished")').parent().hide();
		$('td.ms-formlabel:contains("FI Task finished")').parent().hide();
		$('td.ms-formlabel:contains("KC Task finished")').parent().hide();
		$('td.ms-formlabel:contains("MA Task finished")').parent().hide();
		$('td.ms-formlabel:contains("HR Task finished")').parent().hide();
	}
	
	function showIT() {
		//--disable--
		$('td.ms-formlabel:contains("First Name")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Last Name")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("First Name (with special characters - local alphabet)")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Last Name (with special characters - local alphabet)")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Date of Entry")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Gender")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Agreed Working Hours per Week")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Full-Time Equivalent (FTE)")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("JurXpert Access")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("DMS Access")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Office Adress")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Title")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Additional Title")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Date of Exit")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Last Working Day")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("In-House Function")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Careerpath Level")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Personnel Number")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Planned Admittance to the Bar")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Registration as Attorney")').next().find("input").prop("disabled", true);

		//term store fields + term picker img
		var $jobTitle = $("div[title='Job Title']");
		$jobTitle.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$jobTitle.parent().find('img').hide();
		
		var $professionalCategory = $("div[title='Professional Category']");
		$professionalCategory.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$professionalCategory.parent().find('img').hide();
		
		var $position = $("div[title='Position']");
		$position.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$position.parent().find('img').hide();
		
		var $payrollStatus = $("div[title='Payroll Status']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
		
		var $officeCity = $("div[title='Office City']");
		$officeCity.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$officeCity.parent().find('img').hide();
		
		var $team1 = $("div[title='Team 1']");
		$team1.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$team1.parent().find('img').hide();
		
		var $team2 = $("div[title='Team 2']");
		$team2.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$team2.parent().find('img').hide();
		
		var $office1 = $("div[title='Office 1']");
		$office1.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$office1.parent().find('img').hide();
		
		var $office1id = $("div[title='Office 1 ID']");
		$office1id.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$office1id.parent().find('img').hide();
		
		var $office2 = $("div[title='Office 2']");
		$office2.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$office2.parent().find('img').hide();
		
		var $office2id = $("div[title='Office 2 ID']");
		$office2id.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$office2id.parent().find('img').hide();
		
		var $pg1 = $("div[title='PG 1']");
		$pg1.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$pg1.parent().find('img').hide();
		
		var $pg1id = $("div[title='PG 1 ID']");
		$pg1id.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$pg1id.parent().find('img').hide();
		
		var $pg2 = $("div[title='PG 2']");
		$pg2.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$pg2.parent().find('img').hide();
		
		var $pg2id = $("div[title='PG 2 ID']");
		$pg2id.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$pg2id.parent().find('img').hide();
		
		var $ep1 = $("div[title='EP 1']");
		$ep1.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$ep1.parent().find('img').hide();
		
		var $ep2 = $("div[title='EP 2']");
		$ep2.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$ep2.parent().find('img').hide();
		
		var $team1hr = $("div[title='Team 1 (HR)']");
		$team1hr.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$team1hr.parent().find('img').hide();
		
		var $team2hr = $("div[title='Team 2 (HR)']");
		$team2hr.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$team2hr.parent().find('img').hide();
		
		var $praticeArea = $("div[title='Practice Area']");
		$praticeArea.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$praticeArea.parent().find('img').hide();

		//choice fields
		$('td.ms-formlabel:contains("Employment Status")').next().find("select").prop("disabled", true);
		$('td.ms-formlabel:contains("Team 1 %")').next().find("select").prop("disabled", true);
		$('td.ms-formlabel:contains("Team 2 %")').next().find("select").prop("disabled", true);
		$('td.ms-formlabel:contains("Office 1 %")').next().find("select").prop("disabled", true);
		$('td.ms-formlabel:contains("Office 2 %")').next().find("select").prop("disabled", true);
		$('td.ms-formlabel:contains("PG 1 %")').next().find("select").prop("disabled", true);
		$('td.ms-formlabel:contains("PG 2 %")').next().find("select").prop("disabled", true);
		$('td.ms-formlabel:contains("EP 1 %")').next().find("select").prop("disabled", true);
		$('td.ms-formlabel:contains("EP 2 %")').next().find("select").prop("disabled", true);
		$('td.ms-formlabel:contains("Team 1 % (HR)")').next().find("select").prop("disabled", true);
		$('td.ms-formlabel:contains("Team 2 % (HR)")').next().find("select").prop("disabled", true);
		$('td.ms-formlabel:contains("Additional Position")').next().find("select").prop("disabled", true);
		$('td.ms-formlabel:contains("Careerpath Level")').next().find("select").prop("disabled", true);
		

		//datepicker
		$('td.ms-formlabel:contains("Date of Entry")').next().find(".ms-dtinput > a").hide();
		$('td.ms-formlabel:contains("Date of Exit")').next().find(".ms-dtinput > a").hide();
		$('td.ms-formlabel:contains("Last Working Day")').next().find(".ms-dtinput > a").hide();
		$('td.ms-formlabel:contains("Planned Admittance to the Bar")').next().find(".ms-dtinput > a").hide();
		$('td.ms-formlabel:contains("Registration as Attorney")').next().find(".ms-dtinput > a").hide();
		
		//--remove--
		//fields		
		$('td.ms-formlabel:contains("Date of Birth")').parent().hide();
		$('td.ms-formlabel:contains("Place of Birth")').parent().hide();
		$('td.ms-formlabel:contains("Nationality")').parent().hide();
		$('td.ms-formlabel:contains("Limited Contract Until")').parent().hide();
		$('td.ms-formlabel:contains("Exit Reason")').parent().hide();
		$('td.ms-formlabel:contains("Agreed Currency for Remuneration/Salary")').parent().hide();
		$('td.ms-formlabel:contains("Remuneration per Month (if \\"not employed\\")")').parent().hide();
		$('td.ms-formlabel:contains("Remuneration per Hour (if \\"not employed\\")")').parent().hide();
		$('td.ms-formlabel:contains("Salary (if \\"employed\\")")').parent().hide();
		$('td.ms-formlabel:contains("Salary contains Overtime Payment (\\"all-in contract\\")")').parent().hide();
		$('td.ms-formlabel:contains("BO Task finished")').parent().hide();
		$('td.ms-formlabel:contains("FI Task finished")').parent().hide();
		$('td.ms-formlabel:contains("KC Task finished")').parent().hide();
		$('td.ms-formlabel:contains("MA Task finished")').parent().hide();
		$('td.ms-formlabel:contains("HR Task finished")').parent().hide();
	}
    
    function showKCMA() {
        //--disable--
		//free fields
		$('td.ms-formlabel:contains("First Name")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Last Name")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("First Name (with special characters - local alphabet)")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Last Name (with special characters - local alphabet)")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Title")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Additional Title")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Date of Entry")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Date of Exit")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Last Working Day")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Gender")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Job Title")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Professional Category")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Position")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("In-House Function")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Payroll Status")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Agreed Working Hours per Week")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("Personnel Number")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("User-ID")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("Initials")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("JurXpert #")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("E-Mail Adress")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("Work Phone")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("Mobile Phone")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("Fax")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("Office Adress")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("Planned Admittance to the Bar")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Registration as Attorney")').next().find("input").prop("disabled", true);
		
		//term store fields + term picker img
		var $jobTitle = $("div[title='Job Title']");
		$jobTitle.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$jobTitle.parent().find('img').hide();

		var $professionalCategory = $("div[title='Professional Category']");
		$professionalCategory.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$professionalCategory.parent().find('img').hide();
		
		var $position = $("div[title='Position']");
		$position.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$position.parent().find('img').hide();
		
		var $payrollStatus = $("div[title='Payroll Status']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='Office City']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='Team 1']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='Team 2']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='Office 1']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='Office 1 ID']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='Office 2']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='Office 2 ID']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='PG 1']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='PG 1 ID']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='PG 2']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='PG 2 ID']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='EP 1']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='EP 2']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='Team 1 (HR)']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='Team 2 (HR)']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='Practice Area']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();

		//choice fields
		$('td.ms-formlabel:contains("Employment Status")').next().find("select").prop("disabled", true);
		$('td.ms-formlabel:contains("Additional Position")').next().find("select").prop("disabled", true);
		$('td.ms-formlabel:contains("Careerpath Level")').next().find("select").prop("disabled", true);
        $('td.ms-formlabel:contains("Team 1 %")').next().find("select").prop("disabled", true);
        $('td.ms-formlabel:contains("Team 2 %")').next().find("select").prop("disabled", true);
        $('td.ms-formlabel:contains("Office 1 %")').next().find("select").prop("disabled", true);
        $('td.ms-formlabel:contains("Office 2 %")').next().find("select").prop("disabled", true);
        $('td.ms-formlabel:contains("PG 1 %")').next().find("select").prop("disabled", true);
        $('td.ms-formlabel:contains("PG 2 %")').next().find("select").prop("disabled", true);
        $('td.ms-formlabel:contains("EP 1 %")').next().find("select").prop("disabled", true);
        $('td.ms-formlabel:contains("EP 2 %")').next().find("select").prop("disabled", true);
        $('td.ms-formlabel:contains("Team 1 % (HR)")').next().find("select").prop("disabled", true);
        $('td.ms-formlabel:contains("Team 2 % (HR)")').next().find("select").prop("disabled", true);
		
		//datepicker
		$('td.ms-formlabel:contains("Date of Entry")').next().find(".ms-dtinput > a").hide();
		$('td.ms-formlabel:contains("Date of Exit")').next().find(".ms-dtinput > a").hide();
		$('td.ms-formlabel:contains("Last Working Day")').next().find(".ms-dtinput > a").hide();
        $('td.ms-formlabel:contains("Planned Admittance to the Bar")').next().find(".ms-dtinput > a").hide();
        $('td.ms-formlabel:contains("Registration as Attorney")').next().find(".ms-dtinput > a").hide();
		
        
        //--remove--
        //fields
		$('td.ms-formlabel:contains("Date of Birth")').parent().hide();
		$('td.ms-formlabel:contains("JurXpert Access")').parent().hide();
		$('td.ms-formlabel:contains("DMS Access")').parent().hide();
		$('td.ms-formlabel:contains("Place of Birth")').parent().hide();
		$('td.ms-formlabel:contains("Nationality")').parent().hide();
		$('td.ms-formlabel:contains("Limited Contract Until")').parent().hide();
		$('td.ms-formlabel:contains("Exit Reason")').parent().hide();
		$('td.ms-formlabel:contains("Agreed Currency for Remuneration/Salary")').parent().hide();
		$('td.ms-formlabel:contains("Remuneration per Month (if \\"not employed\\")")').parent().hide();
		$('td.ms-formlabel:contains("Remuneration per Hour (if \\"not employed\\")")').parent().hide();
		$('td.ms-formlabel:contains("Salary (if \\"employed\\")")').parent().hide();
		$('td.ms-formlabel:contains("Salary contains Overtime Payment (\\"all-in contract\\")")').parent().hide();
		$('td.ms-formlabel:contains("R-Code")').parent().hide();
		$('td.ms-formlabel:contains("BO Task finished")').parent().hide();
        $('td.ms-formlabel:contains("IT Task finished")').parent().hide();
		$('td.ms-formlabel:contains("FI Task finished")').parent().hide();
		$('td.ms-formlabel:contains("MA Task finished")').parent().hide();
		$('td.ms-formlabel:contains("HR Task finished")').parent().hide();
        $('td.ms-formlabel:contains("KC Task finished")').parent().hide();
    }
    
    function showFI() {
        //--disable--
		//free fields
		$('td.ms-formlabel:contains("First Name")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Last Name")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("First Name (with special characters - local alphabet)")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Last Name (with special characters - local alphabet)")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Title")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Additional Title")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Date of Entry")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Date of Exit")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Last Working Day")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Gender")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Job Title")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Professional Category")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Position")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("In-House Function")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Payroll Status")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Agreed Working Hours per Week")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("Personnel Number")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("User-ID")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("Initials")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("JurXpert #")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("E-Mail Adress")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("Work Phone")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("Mobile Phone")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("Fax")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("Office Adress")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("Planned Admittance to the Bar")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Registration as Attorney")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("Date of Birth")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("JurXpert Access")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("DMS Access")').next().find("input").prop("disabled", true);
		$('td.ms-formlabel:contains("Place of Birth")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("Limited Contract Until")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("Remuneration per Month (if \\"not employed\\")")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("Remuneration per Hour (if \\"not employed\\")")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("Salary (if \\"employed\\")")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("Salary contains Overtime Payment (\\"all-in contract\\")")').next().find("input").prop("disabled", true);
        $('td.ms-formlabel:contains("R-Code")').next().find("input").prop("disabled", true);
		
		//term store fields + term picker img
		var $jobTitle = $("div[title='Job Title']");
		$jobTitle.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$jobTitle.parent().find('img').hide();

		var $professionalCategory = $("div[title='Professional Category']");
		$professionalCategory.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$professionalCategory.parent().find('img').hide();
		
		var $position = $("div[title='Position']");
		$position.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$position.parent().find('img').hide();
		
		var $payrollStatus = $("div[title='Payroll Status']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='Office City']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='Team 1']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='Team 2']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='Office 1']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='Office 1 ID']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='Office 2']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='Office 2 ID']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='PG 1']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='PG 1 ID']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='PG 2']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='PG 2 ID']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='EP 1']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='EP 2']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='Team 1 (HR)']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='Team 2 (HR)']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='Practice Area']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();
        
        var $payrollStatus = $("div[title='Agreed Currency for Remuneration/Salary']");
		$payrollStatus.children("div[role='textbox']").prop("contenteditable",false).css(disabledTermStoreCSS);
		$payrollStatus.parent().find('img').hide();

		//choice fields
		$('td.ms-formlabel:contains("Employment Status")').next().find("select").prop("disabled", true);
		$('td.ms-formlabel:contains("Additional Position")').next().find("select").prop("disabled", true);
		$('td.ms-formlabel:contains("Careerpath Level")').next().find("select").prop("disabled", true);
        $('td.ms-formlabel:contains("Team 1 %")').next().find("select").prop("disabled", true);
        $('td.ms-formlabel:contains("Team 2 %")').next().find("select").prop("disabled", true);
        $('td.ms-formlabel:contains("Office 1 %")').next().find("select").prop("disabled", true);
        $('td.ms-formlabel:contains("Office 2 %")').next().find("select").prop("disabled", true);
        $('td.ms-formlabel:contains("PG 1 %")').next().find("select").prop("disabled", true);
        $('td.ms-formlabel:contains("PG 2 %")').next().find("select").prop("disabled", true);
        $('td.ms-formlabel:contains("EP 1 %")').next().find("select").prop("disabled", true);
        $('td.ms-formlabel:contains("EP 2 %")').next().find("select").prop("disabled", true);
        $('td.ms-formlabel:contains("Team 1 % (HR)")').next().find("select").prop("disabled", true);
        $('td.ms-formlabel:contains("Team 2 % (HR)")').next().find("select").prop("disabled", true);
        $('td.ms-formlabel:contains("Nationality")').next().find("select").prop("disabled", true);
        $('td.ms-formlabel:contains("Exit Reason")').next().find("select").prop("disabled", true);
		
		//datepicker
		$('td.ms-formlabel:contains("Date of Entry")').next().find(".ms-dtinput > a").remove();
		$('td.ms-formlabel:contains("Date of Exit")').next().find(".ms-dtinput > a").remove();
		$('td.ms-formlabel:contains("Last Working Day")').next().find(".ms-dtinput > a").remove();
        $('td.ms-formlabel:contains("Planned Admittance to the Bar")').next().find(".ms-dtinput > a").remove();
        $('td.ms-formlabel:contains("Registration as Attorney")').next().find(".ms-dtinput > a").remove();
        $('td.ms-formlabel:contains("Date of Birth")').next().find(".ms-dtinput > a").remove();
        $('td.ms-formlabel:contains("Limited Contract Until")').next().find(".ms-dtinput > a").remove();
        
        //--remove--
        //fields
		$('td.ms-formlabel:contains("BO Task finished")').parent().hide();
		$('td.ms-formlabel:contains("IT Task finished")').parent().hide();
		$('td.ms-formlabel:contains("FI Task finished")').parent().hide();
		$('td.ms-formlabel:contains("MA Task finished")').parent().hide();
		$('td.ms-formlabel:contains("HR Task finished")').parent().hide();
        $('td.ms-formlabel:contains("KC Task finished")').parent().hide();
    }
	
	function removeAll() {
		$('td.ms-formlabel:contains("First Name (with special characters - local alphabet)")').parent().remove();
		$('td.ms-formlabel:contains("Last Name (with special characters - local alphabet)")').parent().remove();
		$('td.ms-formlabel:contains("Date of Birth")').parent().remove();
		$('td.ms-formlabel:contains("Date of Entry")').parent().remove();
		$('td.ms-formlabel:contains("Gender")').parent().remove();
		$('td.ms-formlabel:contains("Job Title")').parent().remove();
		$('td.ms-formlabel:contains("Professional Category")').parent().remove();
		$('td.ms-formlabel:contains("Position")').parent().remove();
		$('td.ms-formlabel:contains("Employment Status")').parent().remove();
		$('td.ms-formlabel:contains("Payroll Status")').parent().remove();
		$('td.ms-formlabel:contains("Agreed Working Hours per Week")').parent().remove();
		$('td.ms-formlabel:contains("Full-Time Equivalent (FTE)")').parent().remove();
		$('td.ms-formlabel:contains("JurXpert Access")').parent().remove();
		$('td.ms-formlabel:contains("DMS Access")').parent().remove();
		$('td.ms-formlabel:contains("Office City")').parent().remove();
		$('td.ms-formlabel:contains("Office Adress")').parent().remove();
		$('td.ms-formlabel:contains("Team 1")').parent().remove();
		$('td.ms-formlabel:contains("Team 1 %")').parent().remove();
		$('td.ms-formlabel:contains("Team 2")').parent().remove();
		$('td.ms-formlabel:contains("Team 2 %")').parent().remove();
		$('td.ms-formlabel:contains("Office 1")').parent().remove();
		$('td.ms-formlabel:contains("Office 1 %")').parent().remove();
		$('td.ms-formlabel:contains("Office 1 ID")').parent().remove();
		$('td.ms-formlabel:contains("Office 2")').parent().remove();
		$('td.ms-formlabel:contains("Office 2 %")').parent().remove();
		$('td.ms-formlabel:contains("Office 2 ID")').parent().remove();
		$('td.ms-formlabel:contains("PG 1")').parent().remove();
		$('td.ms-formlabel:contains("PG 1 %")').parent().remove();
		$('td.ms-formlabel:contains("PG 1 ID")').parent().remove();
		$('td.ms-formlabel:contains("PG 2")').parent().remove();
		$('td.ms-formlabel:contains("PG 2 %")').parent().remove();
		$('td.ms-formlabel:contains("EP 1")').parent().remove();
		$('td.ms-formlabel:contains("EP 1 %")').parent().remove();
		$('td.ms-formlabel:contains("EP 2")').parent().remove();
		$('td.ms-formlabel:contains("EP 2 %")').parent().remove();
		$('td.ms-formlabel:contains("Practice Area")').parent().remove();
		$('td.ms-formlabel:contains("Title")').parent().remove();
		$('td.ms-formlabel:contains("Additional Title")').parent().remove();
		$('td.ms-formlabel:contains("Place of Birth")').parent().remove();
		$('td.ms-formlabel:contains("Nationality")').parent().remove();
		$('td.ms-formlabel:contains("Limited Contract Until")').parent().remove();
		$('td.ms-formlabel:contains("Date of Exit")').parent().remove();
		$('td.ms-formlabel:contains("Exit Reason")').parent().remove();
		$('td.ms-formlabel:contains("Last Working Day")').parent().remove();
		$('td.ms-formlabel:contains("In-House Function")').parent().remove();
		$('td.ms-formlabel:contains("Careerpath Level")').parent().remove();
		$('td.ms-formlabel:contains("Agreed Currency for Remuneration/Salary")').parent().remove();
		$('td.ms-formlabel:contains("Remuneration per Month (if \\"not employed\\")")').parent().remove();
		$('td.ms-formlabel:contains("Remuneration per Hour (if \\"not employed\\")")').parent().remove();
		$('td.ms-formlabel:contains("Salary (if \\"employed\\")")').parent().remove();
		$('td.ms-formlabel:contains("Salary contains Overtime Payment (\\"all-in contract\\")")').parent().remove();
		$('td.ms-formlabel:contains("Personnel Number")').parent().remove();
		$('td.ms-formlabel:contains("Planned Admittance to the Bar")').parent().remove();
		$('td.ms-formlabel:contains("Registration as Attorney")').parent().remove();
		$('td.ms-formlabel:contains("User-ID")').parent().remove();
		$('td.ms-formlabel:contains("Initials")').parent().remove();
		$('td.ms-formlabel:contains("JurXpert #")').parent().remove();
		$('td.ms-formlabel:contains("R-Code")').parent().remove();
		$('td.ms-formlabel:contains("E-Mail Adress")').parent().remove();
		$('td.ms-formlabel:contains("Work Phone")').parent().remove();
		$('td.ms-formlabel:contains("Mobile Phone")').parent().remove();
		$('td.ms-formlabel:contains("Fax")').parent().remove();
		$('td.ms-formlabel:contains("BO Task finished")').parent().hide();
		$('td.ms-formlabel:contains("IT Task finished")').parent().hide();
		$('td.ms-formlabel:contains("FI Task finished")').parent().hide();
		$('td.ms-formlabel:contains("KC Task finished")').parent().hide();
		$('td.ms-formlabel:contains("MA Task finished")').parent().hide();
		$('td.ms-formlabel:contains("HR Task finished")').parent().hide();
	}
}